import bot, map, evolve
import pygame
import math
import random
import time

pygame.init()
WIDTH = 1600
HEIGTH = 800
screenSize = (WIDTH,HEIGTH)
pind = pygame.display.set_mode( screenSize )
pygame.display.set_caption("Neurons")
pind.fill( (255,255,255) )
pygame.display.flip()
numberOfBotSets = 40
fightTime = 10
inspect = 0
gen = 0
botLstL = []
botLstR = []

for i in range(numberOfBotSets):
    botLstL.append(bot.bot("left", 1))
    botLstR.append(bot.bot("right", 1))

global bulletLst
bulletLst = []

def draw(s):


    botLstR[inspect].draw(s)
    botLstL[inspect].draw(s)

    for b in bulletLst:
        if b.pairN == inspect:
            b.draw(s)

    map.drawMap(WIDTH, HEIGTH, s, gen, (botLstL, botLstR), time0, inspect, fightTime)

def update():
    pind.fill((255, 255, 255))
    global bulletLst
    bulletLst = bot.botLstUptate(botLstR, bulletLst, botLstL)
    bulletLst = bot.botLstUptate(botLstL, bulletLst, botLstR)

    for b in bulletLst:
        b.update()
        if (b.pos[0] < 0 or b.pos[0] > WIDTH*0.7 or b.pos[1] < 0 or b.pos[1] > HEIGTH):
            bulletLst.remove(b)


time0 = time.clock()

while True:
    event = pygame.event.poll()
    # -------------------------------------------------------------
    # draw here
    # -------------------------------------------------------------

    draw(pind)

    pygame.display.flip()

    update()
    if time.clock() - time0 >= fightTime:
        time0 = time.clock()
        bulletLst = []
        gen += 1
        time1 = time.clock()
        botLstL, botLstR = evolve.evolve(botLstL, botLstR)