import random
import pygame
import math
import time
import bullet

class bot():

    def __init__(self, side, brain):
        self.side = side
        if self.side == "left":
            self.pos = (200, 400)

        elif self.side == "right":
            self.pos = (1000, 400)

        self.radius = 20
        self.eyeRadius = 5
        self.visionSpread = math.radians(30)
        self.vRange = 1000
        self.structure = [5,7,4]
        if brain == 1:
            self.brain = self.createBrain()

        else:
            self.brain = brain

        self.inputs = []
        self.outputs = []
        for i in range(self.structure[0]):
            self.inputs.append(0)

        for i in range(self.structure[len(self.structure)-1]):
            self.outputs.append(0)

        self.score = 0
        self.XSpeed = 0
        self.YSpeed = 0
        self.direction = math.atan2(self.XSpeed, self.YSpeed)
        self.timeLastShot = 0
        self.reloadTime = 1
        self.maxSpeed = 3
        self.maxRotationSpeed = 0.1
        self.shoot = False



    def draw(self,s):
        pygame.draw.circle(s, (0, 0, 0),(round(self.pos[0]), round(self.pos[1])), self.radius)
        r = self.radius - self.eyeRadius
        eyePos = (round(self.pos[0] + r*math.cos(self.direction)), round(self.pos[1] + r*math.sin(self.direction)))

        pygame.draw.circle(s, (255, 0, 0), eyePos, self.eyeRadius)
        pygame.draw.line(s, (255, 0, 0), eyePos,
                         (round(eyePos[0] + self.vRange * math.cos(self.direction + self.visionSpread / 2)),
                          round(eyePos[1] + self.vRange * math.sin(self.direction + self.visionSpread / 2))), 1)
        pygame.draw.line(s, (255, 0, 0), eyePos,
                         (round(eyePos[0] + self.vRange * math.cos(self.direction - self.visionSpread / 2)),
                          round(eyePos[1] + self.vRange * math.sin(self.direction - self.visionSpread / 2))), 1)

    def update(self, otherBot):
        #inputs: Xspeed, Yspeed, visionSpread, eye, distance form enemy, bias
        if self.eye(otherBot) == 1:
            self.inputs = [self.XSpeed, self.YSpeed, self.visionSpread,
                           math.atan2(otherBot.pos[1] - self.pos[1], otherBot.pos[0] - self.pos[0]),
                           math.sqrt((self.pos[0]-otherBot.pos[0])**2+(self.pos[1]-otherBot.pos[1])**2)/100, 1]
        else:
            self.inputs = [self.XSpeed, self.YSpeed, self.visionSpread, 0, 0, 1]
        self.outputs = self.calculateOutputs(self.inputs)
        #self.Xacc = self.outputs[0]*2
        #self.Yacc = self.outputs[1]*2
        self.speed = abs(self.outputs[0]*100)
        directionD = self.outputs[1]
        if self.speed > self.maxSpeed:
            self.speed = self.maxSpeed

        elif self.speed < -self.maxSpeed:
            self.speed = -self.maxSpeed

        if directionD > self.maxRotationSpeed:
            directionD = self.maxRotationSpeed

        elif directionD < -self.maxRotationSpeed:
            directionD = -self.maxRotationSpeed

        self.direction += directionD
        if abs(self.direction) > math.pi:
            self.direction = abs(self.direction - 2 * math.pi)

        self.vRangeD = self.outputs[2]
        self.shootF = self.outputs[3]
        """
        #self.XSpeed = self.outputs[0]
        #self.YSpeed = self.outputs[1]

        if self.XSpeed >= self.maxSpeed:
            self.XSpeed = self.maxSpeed
        elif self.XSpeed <= -self.maxSpeed:
            self.XSpeed = -self.maxSpeed
        if self.YSpeed >= self.maxSpeed:
            self.YSpeed = self.maxSpeed
        elif self.YSpeed <= -self.maxSpeed:
            self.YSpeed = -self.maxSpeed



        #Movement

        self.XSpeed += self.Xacc
        self.YSpeed += self.Yacc
        """
        self.XSpeed = self.speed * math.cos(self.direction)
        self.YSpeed = self.speed * math.sin(self.direction)
        #self.direction = math.atan2(self.YSpeed, self.XSpeed)


        WIDTH = 1600
        HEIGTH = 800
        if self.side == "left":
            #left wall
            if self.XSpeed + self.pos[0] < self.radius:
                self.pos = (self.radius, self.pos[1])

            #right wall
            elif self.XSpeed + self.pos[0] > WIDTH * 0.3 - self.radius:
                self.pos = (WIDTH * 0.3 - self.radius, self.pos[1])

            else:
                self.pos = (self.pos[0] + self.XSpeed, self.pos[1])

        if self.side == "right":
            # left wall
            if self.XSpeed + self.pos[0] < WIDTH * 0.4 + self.radius:
                self.pos = (WIDTH * 0.4 + self.radius, self.pos[1])

            # right wall
            elif self.XSpeed + self.pos[0] > WIDTH * 0.7 - self.radius:
                self.pos = (WIDTH * 0.7 - self.radius, self.pos[1])

            else:
                self.pos = (self.pos[0] + self.XSpeed, self.pos[1])

        # N wall
        if self.YSpeed + self.pos[1] < self.radius:
            self.pos = (self.pos[0], self.radius)

        # S wall
        elif self.YSpeed + self.pos[1] > HEIGTH - self.radius:
            self.pos = (self.pos[0], HEIGTH - self.radius)

        else:
            self.pos = (self.pos[0], self.pos[1] + self.YSpeed)

        #vision field
        if self.visionSpread + self.vRangeD <= math.radians(1):
            self.visionSpread = math.radians(1)

        elif self.visionSpread + self.vRangeD >= math.radians(75):
            self.visionSpread = math.radians(75)

        else:
            self.visionSpread += self.vRangeD

        #Shooting

        if time.clock() - self.timeLastShot > self.reloadTime and self.shootF >= 0:
            self.timeLastShot = time.clock()
            self.shoot = True

    def eye(self, other):
        mousePos = pygame.mouse.get_pos()
        a = math.atan2(other.pos[1] - self.pos[1], other.pos[0] - self.pos[0])
        #a = math.atan2(mousePos[1] - self.pos[1], mousePos[0] - self.pos[0]) #for debugging

        #removing extra spins


        if (abs(a - self.direction) < self.visionSpread/2) or (math.pi * 2 - abs(a - self.direction) < self.visionSpread/2):
            self.score += 0.0001 / self.visionSpread
            return 1

        else:
            return -1

    def createBrain(self):
        # createse a list of weights between neurons
        brain = []
        for i in range(len(self.structure)-1):
            weightLayer = []
            for j in range(self.structure[i+1]):
                weights = []
                for k in range(self.structure[i]+1):
                    weights.append(random.randint(-120,120)/1000)
                weightLayer.append(weights)
            brain.append(weightLayer)
        return brain

    def calculateOutputs(self, inputs):

        for i in range(len(self.structure)-1):
            if i == 0:
                inputs.append(1)
                neuronLayer = inputs

            else:
                nNeuronLayer.append(1)
                neuronLayer = nNeuronLayer

            nNeuronLayer = []
            for j in range(self.structure[i+1]):
                neuron = 0
                for k in range(self.structure[i]):
                    neuron += neuronLayer[k] * self.brain[i][j][k]

                nNeuronLayer.append(neuron)

        return nNeuronLayer

    def __repr__(self):
        return repr((self.score))

def botLstUptate(botLst, bulletLst, otherBotLst):

    for pairN in range(len(botLst)):
        b = botLst[pairN]
        b.update(otherBotLst[pairN])
        if b.shoot:

            d = b.direction + random.randint(-round(b.visionSpread*1000), round(b.visionSpread*1000))/2000
            bulletLst.append(bullet.bullet((b.pos[0] + b.radius * math.cos(d),
                                            (b.pos[1] + b.radius * math.sin(d))), d, pairN))
            b.shoot = False

        #collision check
        for i in bulletLst:
            if i.canCollide and i.pairN == pairN:
                distance = math.sqrt((b.pos[0]-i.pos[0])**2+(b.pos[1]-i.pos[1])**2)
                l = b.radius + i.radius
                if distance < l:
                    otherBotLst[pairN].score += 10
                    b.score -= 10
                    bulletLst.remove(i)
    return bulletLst

