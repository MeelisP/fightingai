import pygame
import time

def drawMap(WIDTH, HEIGTH, s, gen, bots, time0, currentPairN, fightTime):




    pygame.draw.line(s, (0, 0, 0), (WIDTH * 0.3, 0), (WIDTH * 0.3, HEIGTH), 1)
    pygame.draw.line(s, (0, 0, 0), (WIDTH * 0.35, 0), (WIDTH * 0.35, HEIGTH), 1)
    pygame.draw.line(s, (0, 0, 0), (WIDTH * 0.4, 0), (WIDTH * 0.4, HEIGTH), 1)
    pygame.draw.rect(s, (125, 125, 125), (WIDTH * 0.7, 0, WIDTH, HEIGTH))

    #text
    fontobject = pygame.font.SysFont('Arial', 18)
    s.blit(fontobject.render("Gen: " + str(gen), 1, (0, 205, 0)), (WIDTH * 0.7 + 30, 30))
    s.blit(fontobject.render("Time: " + str(round(fightTime  - (time.clock() - time0))), 1, (0, 205, 0)), (WIDTH * 0.7 + 30, 50))

    for j in range(len(bots[0])):
        s.blit(fontobject.render(str(j + 1)+": " + str(round(bots[0][j].score)), 1, (0, 205, 0)), (WIDTH * 0.3 + 10, j * 20))
        s.blit(fontobject.render(str(j + 1)+": " + str(round(bots[1][j].score)), 1, (0, 205, 0)), (WIDTH * 0.35 + 10,  j * 20))

        #drawing neural networks
    fontobject2 = pygame.font.SysFont('Arial', 12)

    for i in range(len(bots)):
        for j in range(len(bots[i][currentPairN].structure)):
            for k in range(bots[i][currentPairN].structure[j]):
                m = len(bots[i][currentPairN].structure)

                if j == 0:
                    #draws input numbers
                    pygame.draw.circle(s, (255, 255, 255), (round(WIDTH * 0.72 + m * 65 * i + 50 * j), round(HEIGTH * 0.3 + 50 * k)), 15)
                    s.blit(fontobject2.render(str("{0:.1f}".format(bots[i][currentPairN].inputs[k])), 1, (0, 0, 0)),
                    (round(WIDTH * 0.72 + m * 65 * i - 8 + 50 * j), round(HEIGTH * 0.3 + 50 * k - 8)))


                elif j == len(bots[i][currentPairN].structure)-1:
                    #draws output numbers
                    pygame.draw.circle(s, (255, 255, 255), (round(WIDTH * 0.72 + m * 65 * i  + 50 * j), round(HEIGTH * 0.3 + 50 * k)), 15)
                    s.blit(fontobject2.render(str("{0:.3f}".format(bots[i][currentPairN].outputs[k])), 1, (0, 0, 0)),
                    (round(WIDTH * 0.72 + m * 65 * i - 12 + 50 * j), round(HEIGTH * 0.3 + 50 * k - 8)))


                else:
                    pygame.draw.circle(s, (255, 255, 255), (round(WIDTH * 0.72 + m * 65 * i + 50 * j), round(HEIGTH * 0.3 + 50 * k)), 5)