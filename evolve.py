import bot
import random

def evolve(botLst1, botLst2):
    botLst1 = sorted(botLst1, key=lambda bot: bot.score, reverse=True)
    botLst2 = sorted(botLst2, key=lambda bot: bot.score, reverse=True)
    botLst1 = botLst1[:10]
    botLst2 = botLst2[:10]
    bots = []
    for i in [botLst1, botLst2]:
        nbotsLst = []
        for j in range(0, len(i), 2):
            brain1 = i[j].brain
            brain2 = i[j + 1].brain
            brainstructure = i[j].structure      # [4,5,5,4]
            for k in range(8):
                newBrain = []
                for x in range(len(brain1)):
                    layer = []
                    for y in range(len(brain1[x])):
                        weigths = []
                        for z in range(len(brain1[x][y])):
                            a = random.random()
                            if a <= 0.4:
                                if random.random() < 0.85:
                                    weigths.append(brain1[x][y][z])

                                else:
                                    weigths.append(brain1[x][y][z] + random.randint(-100, 100) / 1000)

                            elif a >= 0.6:
                                if random.random() < 0.85:
                                    weigths.append(brain2[x][y][z])

                                else:
                                    weigths.append(brain2[x][y][z] + random.randint(-100, 100) / 1000)

                            else:
                                weigths.append((brain1[x][y][z] + brain2[x][y][z])/2)

                        layer.append(weigths)
                    newBrain.append(layer)
                nbotsLst.append(bot.bot(i[j].side, newBrain))
        bots.append(nbotsLst)
    return bots