import pygame
import math
import time

class bullet:

    def __init__(self, pos, direction, pairN):

        self.pos = pos
        self.speed = 5
        self.XSpeed = self.speed * math.cos(direction)
        self.YSpeed = self.speed * math.sin(direction)
        self.radius = 5
        self.canCollide = False
        self.time0 = time.clock()
        self.timeTillCol = 0.2
        self.pairN = pairN

    def draw(self, s):

        pygame.draw.circle(s, (0, 0, 0), (round(self.pos[0]), round(self.pos[1])), self.radius)

    def update(self):

        self.pos = (self.pos[0] + self.XSpeed, self.pos[1] + self.YSpeed)
        if time.clock() - self.time0 > self.timeTillCol:
            self.canCollide = True